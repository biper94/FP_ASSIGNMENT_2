#include "DBHelper.h"

DBHelper::DBHelper()
	: mStudentSize(0)
	, mStudentData(nullptr) {
	mHashMap = new HashMap();
	mBplusTree = new BPT();
	mFileDB.open("Student.DB", ios::binary | ios::out);
}

void DBHelper::GetDataFromFile() {
	ifstream fin(INPUT_FILE_NAME);
	if (fin.fail()) {
		cout << "Could not find " << INPUT_FILE_NAME << endl;
		exit(0);
	}
	string sData;
	stringstream ss;

	getline(fin, sData);
	ss.str(sData);
	ss >> mStudentSize;
	mStudentData = new StudentInfo[mStudentSize];
	for (int i = 0; i < mStudentSize; i++) {
		string sData2;
		stringstream ss2;
		string split_by_comma[4];
		int stdID;
		float score;

		getline(fin, sData2);
		ss2.str(sData2);
		SplitByComma(sData2, split_by_comma);
		stdID = atoi(split_by_comma[1].c_str());
		score = atof(split_by_comma[2].c_str());
		mHashMap->Insert(stdID);
		mBplusTree->insert_Record(score, mHashMap->GetBlockNumber(stdID));
		
		mStudentData[i] = StudentInfo(sData2);
	}
	int k;
	
	//mHashMap->PrintHashTable();
	mBplusTree->CreateIdxFile();
	cout << "Enter the integer showing k'th leaf node : ";
	cin >> k;
	mBplusTree->showKthLeaf(k);
}
void DBHelper::CreateDBFile() {
	int tableSize = mHashMap->mTableSize;
	int* countArr = new int[tableSize];
	fill_n(countArr, tableSize, 0);

	mFileDB.seekp(0);
	for (int i = 0; i < mStudentSize; i++) {
		
		StudentInfo std = mStudentData[i];
		int blockNum = mHashMap->GetBlockNumber(std.mStudentID);
		if (blockNum != -1) {
			int pos = blockNum * 4096 + countArr[blockNum] * (NAMELENGTH + 12);
			mFileDB.seekp(pos);

			mFileDB.write((char*)&std.mName, sizeof(std.mName));
			mFileDB.write((char*)&std.mStudentID, sizeof(std.mStudentID));
			mFileDB.write((char*)&std.mScore, sizeof(std.mScore));
			mFileDB.write((char*)&std.mAdvisorID, sizeof(std.mAdvisorID));
			countArr[blockNum]++;
		}
		
	}
	
	delete[] countArr;
}
void DBHelper::CreateHashFile() {
	mHashMap->CreateHashFile();
}
void DBHelper::CreateIndexFile() {
	mBplusTree->CreateIdxFile();
}

DBHelper::~DBHelper() {
	delete mHashMap;
	delete[] mStudentData;
	delete mBplusTree;
	mFileDB.close();
}