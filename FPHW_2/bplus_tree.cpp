#include <iostream>
#include <list>
#include <iterator>
#include "bplus_tree.h"



BPT_Node::BPT_Node(bool _leaf, bool _root, float _score, int _block) {
	leaf = _leaf;
	root = _root;
	score.push_back(_score);
	blockNum.push_back(_block);
	entry_num = 1;

}
int BPT_Node::insert_Entry(float _score, int _block) {
	int cn = 0;
	list<float>::iterator it = score.begin();
	list<int>::iterator bit = blockNum.begin();
	while (it != score.end()) {
		if (*it < _score) {
			it++;
			bit++;
			cn++;
		}
		else {
			break;
		}
	}
	score.insert(it, _score);
	blockNum.insert(bit, _block);
	entry_num++;
	return cn;
}BPT::BPT() 
	: root(nullptr){
	mFileIDX.open("Students_score.idx", ios::binary | ios::out);
}
BPT::BPT(float _score, int _block) {
	root = new BPT_Node(true, true, _score, _block);
	mFileIDX.open("Students_score.idx", ios::binary | ios::out);
}


BPT_Node*  BPT::find_Left(BPT_Node* tmp) {
	if (tmp->leaf) {
		return tmp;
	}
	else {
		return find_Left(*tmp->child.begin());
	}
}

BPT_Node* BPT::find_Node(float _score) {
	BPT_Node* tmp = find_Left(root);
	list<float>::iterator it = tmp->score.begin();
	BPT_Node* prev = NULL;
	while (tmp != nullptr) {
		it = tmp->score.end();
		it--;
		if (*it < _score) {
			prev = tmp;
			tmp = tmp->next;
			if (tmp != nullptr && *(tmp->score.begin()) > _score) {
				return prev;
			}
			
		}
		else {
			return tmp;
		}
	}
	return prev;
}

void  BPT::split_internalNode(BPT_Node *tmp) {
	// score split
	list<float> l_score;
	list<float> r_score;
	list<float>::iterator s_lit = tmp->score.begin();
	advance(s_lit, M);
	float key = *s_lit;
	Int_ListSplice(&(tmp->score), &l_score, &r_score);

	// blockNum split
	list<int> l_block;
	list<int> r_block;
	list<int>::iterator b_lit = tmp->blockNum.begin();
	advance(b_lit, M);
	int _block = *b_lit;
	Int_ListSplice(&(tmp->blockNum), &l_block, &r_block);

	// child split
	list<BPT_Node*> l_child;
	list<BPT_Node*> r_child;
	list<BPT_Node*>::iterator c_lit = tmp->child.begin();
	advance(c_lit, M + 1);
	Node_ListSplice(&(tmp->child), &l_child, &r_child);

	// split left & right node
	tmp->score = l_score;
	tmp->entry_num = l_score.size();
	tmp->child = l_child;
	tmp->blockNum = l_block;
	BPT_Node *r_node = new BPT_Node(false, false, 0, 0);
	r_node->score = r_score;
	r_node->entry_num = r_score.size();
	r_node->child = r_child;
	r_node->blockNum = r_block;
	

	list<BPT_Node*>::iterator cit = r_node->child.begin();
	for (cit = r_node->child.begin(); cit != r_node->child.end(); cit++) {
		BPT_Node* Ctmp;
		Ctmp = *cit;
		Ctmp->parent = r_node;
	}


	if (tmp->root) {
		tmp->root = false;
		BPT_Node *new_root = new BPT_Node(false, true, key, _block);
		root = new_root;
		tmp->parent = new_root;
		r_node->parent = new_root;
		new_root->child.push_back(tmp);
		new_root->child.push_back(r_node);
	}
	else {
		int c_pos = tmp->parent->insert_Entry(key, _block);
		r_node->parent = tmp->parent;
		c_lit = tmp->parent->child.begin();
		advance(c_lit, c_pos);
		if (tmp->parent->root) {
			c_lit++;
		}
		if (c_lit != tmp->parent->child.end()) {
			if (*c_lit == tmp && *(tmp->score.begin()) < key) {
				c_lit++;
			}
		}
		tmp->parent->child.insert(c_lit, r_node);
		if (tmp->parent->entry_num > BF) {
			split_internalNode(tmp->parent);
		}
	}
}


void BPT::insert_Record(float _score, int _block) {
	if (root != nullptr) {
		BPT_Node* seat = find_Node(_score);
		seat->insert_Entry(_score, _block);
		if (seat->entry_num > BF) {
			split_leafNode(seat);
		}
	}
	else {
		root = new BPT_Node(true, true, _score, _block);
	}
	
}

void BPT::showKthLeaf(int k) {
	BPT_Node *want = find_Left(root);
	for (int i = 0; i < k - 1; i++) {
		if (want->next != NULL)
			want = want->next;
		else {
			cout << "It's not exist!" << endl;
			break;
		}
	}
	list<float>::iterator it = want->score.begin();
	list<int>::iterator bit = want->blockNum.begin();
	int cnt = 1;
	cout << "***" << k << "'th Leaf Node ***" << endl;
	for (it = want->score.begin(); it != want->score.end(); it++) {
		printf("|Score: %.2f, Block: %2d|   ", *it, *bit);
		bit++;
		if (cnt % 3  == 0) 
			cout << endl;
		cnt++;
	}
	cout << endl;
}

void BPT::split_leafNode(BPT_Node *tmp) {
	list<float> l_score;
	list<float> r_score;
	list<float>::iterator s_lit = tmp->score.begin();
	advance(s_lit, M);
	float key = *s_lit;
	Int_ListSplice(&(tmp->score), &l_score, &r_score);

	list<int> l_block;
	list<int> r_block;
	list<int>::iterator b_lit = tmp->blockNum.begin();
	advance(b_lit, M);
	int _block = *b_lit;
	Int_ListSplice(&(tmp->blockNum), &l_block, &r_block);
	
	if (tmp->root) {
		BPT_Node *new_root = new BPT_Node(false, true, key, _block);
		BPT_Node *new_r = new BPT_Node(true, false, key, _block);

		tmp->root = false;
		tmp->score.assign(l_score.begin(), l_score.end());
		tmp->blockNum.assign(l_block.begin(), l_block.end());
		tmp->entry_num = l_score.size();
		tmp->parent = new_root;
		tmp->next = new_r;

		root = new_root;
		new_r->parent = new_root;
		new_r->score.assign(r_score.begin(), r_score.end());
		new_r->blockNum.assign(r_block.begin(), r_block.end());
		new_r->entry_num = r_score.size();

		new_root->child.push_back(tmp);
		new_root->child.push_back(new_r);
		
	}
	else {
		BPT_Node *new_r = new BPT_Node(true, false, key, _block);
		new_r->parent = tmp->parent;
		new_r->score.assign(r_score.begin(), r_score.end());
		new_r->blockNum.assign(r_block.begin(), r_block.end());
		new_r->entry_num = r_score.size();
		new_r->next = tmp->next;

		tmp->score.assign(l_score.begin(), l_score.end());
		tmp->blockNum.assign(l_block.begin(), l_block.end());
		tmp->next = new_r;
		tmp->entry_num = l_score.size();

		list<BPT_Node*>::iterator c_lit = tmp->parent->child.begin();
		int c_pos = tmp->parent->insert_Entry(key, _block);
		advance(c_lit, c_pos);
		if (tmp->parent->root) {
			c_lit++;
		}
		if (c_lit != tmp->parent->child.end()) {
			if (*c_lit == tmp && *(tmp->score.begin()) < key) {
				c_lit++;
			}
		}
		new_r->parent->child.insert(c_lit, new_r);

		if (tmp->parent->entry_num > BF) {
			split_internalNode(tmp->parent);
		}
	}
}

void BPT::PrintLeaf() {
	BPT_Node *left = find_Left(root);
	while (left != nullptr) {
		list<float>::iterator lit = left->score.begin();
		list<int>::iterator bit = left->blockNum.begin();
		for (lit = left->score.begin(); lit != left->score.end(); lit++) {
			cout << "score : " << *lit << " block : " << *bit << "\t";
			bit++;
		}
		cout << endl;
		left = left->next;
	}
}

template <typename T>
void Int_ListSplice(list<T>* score, list<T> *l_score, list<T> *r_score) {
	list<T>::iterator lit = score->begin();
	for (int i = 0; i < score->size(); i++) {
		if (i < M) {
			l_score->push_back(*lit);
		}
		else {
			r_score->push_back(*lit);
		}
		lit++;
	}
}

void Node_ListSplice(list<BPT_Node*>* child, list<BPT_Node*> *l_child, list<BPT_Node*> *r_child) {
	list<BPT_Node*>::iterator lit = child->begin();
	int num;
	if (child->size() == BF+1) {
		num = M;
	}
	else {
		num = M + 1;
	}
	for (int i = 0; i < child->size(); i++) {
		if (i < num) {
			l_child->push_back(*lit);
		}
		else {
			r_child->push_back(*lit);
		}
		lit++;
	}
}

void BPT::Print_Tree() {
	cout << "Print Root Node " << endl;
	Print_Node(root);
}

void BPT::Print_Node(BPT_Node* tmp) {
	if (tmp->leaf) {
		cout << "Print Leaf Node" << endl;
		list<float>::iterator lit = tmp->score.begin();
		list<int>::iterator bit = tmp->blockNum.begin();
		for (lit = tmp->score.begin(); lit != tmp->score.end(); lit++) {
			cout << "score : " << *lit << " block : " << *bit << "\t";
			bit++;
		}
		cout << endl;
		cout << "------------------------------" << endl;
	}
	else {
		cout << "Print InterNal Node" << endl;
		list<float>::iterator lit = tmp->score.begin();
		for (lit = tmp->score.begin(); lit != tmp->score.end(); lit++) {
			cout << *lit << "  ";
		}
		cout << endl;
		cout << "------------------------------" << endl;
		list<BPT_Node*>::iterator cit = tmp->child.begin();
		for (cit = tmp->child.begin(); cit != tmp->child.end(); cit++) {
			Print_Node(*cit);
		}
	}
}

void BPT::CreateIdxFile() {
	BPT_Node* entry = find_Left(root);

	
	
	while (entry != nullptr) {
		list<float>::iterator lit = entry->score.begin();
		list<int>::iterator bit = entry->blockNum.begin();
		for (lit = entry->score.begin(); lit != entry->score.end(); lit++) {
			mFileIDX.write((char*)&(*lit), sizeof(*lit));
			mFileIDX.write((char*)&(*bit), sizeof(*bit));
			bit++;
		}
		entry = entry->next;
	}
}
BPT::~BPT() {
	RecursiveDestructor(root);
	mFileIDX.close();
}

void BPT::RecursiveDestructor(BPT_Node* _node) {
	if (_node != nullptr) {
		list<BPT_Node*>::iterator it;
		for (it = _node->child.begin(); it != _node->child.end(); it++) {
			RecursiveDestructor(*it);
		}
		delete _node;
	}
}
