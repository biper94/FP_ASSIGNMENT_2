#include <iostream>
#include "DBHelper.h"

using namespace std;

int main() {
	DBHelper* db = new DBHelper();
	db->GetDataFromFile();
	db->CreateHashFile();
	db->CreateDBFile();
	db->CreateIndexFile();
}