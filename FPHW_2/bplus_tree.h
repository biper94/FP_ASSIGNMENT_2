#include <iostream>
#include <list>
#include <iterator>
#include <fstream>
#define BF 4096/32

#if BF%2
#define M BF/2
#else
#define M BF/2+1
#endif

#ifndef BPLUS_TREE_H_INCLUDED
#define BPLUS_TREE_H_INCLUDED

using namespace std;

class BPT_Node {
public:
	list<float> score;
    list<int> blockNum;
	int entry_num = 0;
	bool root = false;
	bool leaf = false;
	BPT_Node *next = NULL;	// only leaf
	BPT_Node *parent = NULL;
	list<BPT_Node*> child;   // only internal

	BPT_Node(bool _leaf, bool _root, float _score, int _block);
	int insert_Entry(float _score, int _block);
};

class BPT {
public:
	BPT_Node* root;
	ofstream mFileIDX;
	BPT();
	BPT(float _score, int _block);
	~BPT();
	void RecursiveDestructor(BPT_Node* _node);
	BPT_Node* find_Left(BPT_Node* tmp);
	BPT_Node* find_Node(float _score);
	void split_leafNode(BPT_Node *tmp);
	void split_internalNode(BPT_Node *tmp);
	void insert_Record(float _score, int _block);
	void showKthLeaf(int k);
	void PrintLeaf();
	void Print_Tree();
	void Print_Node(BPT_Node* tmp);
	void CreateIdxFile();
};
template <typename T>
void Int_ListSplice(list<T>* score,list<T> *l_score, list<T> *r_score);

void Node_ListSplice(list<BPT_Node*>* child, list<BPT_Node*> *l_child, list<BPT_Node*> *r_child);



#endif // BPLUS_TREE_H_INCLUDED
