#ifndef DBHELPER_H
#define DBHELPER_H

#include "DynamicHash.h"
#include "StudentInfo.h"
#include "bplus_tree.h"

class DBHelper {
private:
	int mStudentSize;
	StudentInfo* mStudentData;
	HashMap* mHashMap;
	BPT* mBplusTree;
	fstream mFileDB;
	

public:
	DBHelper();
	~DBHelper();
	void GetDataFromFile();
	void CreateHashFile();
	void CreateIndexFile();
	void CreateDBFile();
};

#endif // DBHELPER_H